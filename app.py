# -*- coding: utf-8 -*-
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

from main import get_data, obcine_options, fiter_by_id

app = dash.Dash(external_stylesheets=[dbc.themes.LUX])
server = app.server
OBCINE = obcine_options()
YEARS = [
    2021,
    2020,
    2019,
    2018,
    2017,
    2016,
    2015,
    2014,
    2013,
    2012,
    2011,
    2010,
    2009,
    2008,
    2007,
]


def datatable(data_type, obcina, year):
    df, columns = get_data(data_type, year)
    df = df[df["obcina"].isin(obcina)]
    return dash_table.DataTable(
        id="datatable-interactivity",
        columns=columns,
        data=df.to_dict("records"),
        filter_action="native",
        sort_action="native",
        sort_mode="multi",
        page_action="native",
        page_current=0,
        page_size=100,
        style_table={"overflowX": "auto"},
        style_cell={
            "whiteSpace": "normal",
            "height": "auto",
        },
    )


def contract_info(contract, year):
    posel, zemljisce, stavba = fiter_by_id(contract, year)
    posel.drop("ID Posla", axis=1, inplace=True, errors="ignore")
    stavba.drop("ID Posla", axis=1, inplace=True, errors="ignore")
    zemljisce.drop("ID Posla", axis=1, inplace=True, errors="ignore")
    return [
        html.Div(
            [
                dbc.Row(
                    dash_table.DataTable(
                        columns=[{"name": i, "id": i} for i in posel.columns],
                        data=posel.to_dict("records"),
                        style_table={"maxWidth": "1100px", "overflowX": "auto"},
                        style_cell={
                            "whiteSpace": "normal",
                            "height": "auto",
                            "lineHeight": "15px",
                        },
                    ),
                    justify="center",
                ),
                dbc.Row(html.H4("Stavbe:")),
                dbc.Row(
                    dash_table.DataTable(
                        columns=[{"name": i, "id": i} for i in stavba.columns],
                        data=stavba.to_dict("records"),
                        style_table={
                            "maxWidth": "1100px",
                            "overflowX": "auto",
                        },
                        style_cell={
                            "whiteSpace": "normal",
                            "height": "auto",
                            "lineHeight": "15px",
                        },
                    ),
                    justify="center",
                ),
                dbc.Row(html.H4("Zemljisca:")),
                dbc.Row(
                    dash_table.DataTable(
                        columns=[{"name": i, "id": i} for i in zemljisce.columns],
                        data=zemljisce.to_dict("records"),
                        style_table={"maxWidth": "1100px", "overflowX": "auto"},
                        style_cell={
                            "whiteSpace": "normal",
                            "height": "auto",
                            "lineHeight": "15px",
                        },
                    ),
                    justify="center",
                ),
            ]
        )
    ]


app.layout = dbc.Container(
    [
        dbc.Row(html.H1("Kupo-prodajni posli v Sloveniji"), justify="center"),
        dbc.Row(
            [
                dbc.Col(
                    dcc.Dropdown(
                        id="tip",
                        placeholder="Tip nepremičnine",
                        options=[
                            {"label": "Stavbe", "value": "stavbe"},
                            {"label": "Zemljišča", "value": "zemljisca"},
                        ],
                    ),
                ),
                dbc.Col(
                    dcc.Dropdown(
                        id="obcina", placeholder="Občina", options=OBCINE, multi=True
                    )
                ),
                dbc.Col(
                    dcc.Dropdown(
                        id="year",
                        placeholder="Leto",
                        options=[{"label": x, "value": x} for x in YEARS],
                    )
                ),
                dbc.Col(
                    dbc.Button(
                        "Refresh", id="refresh", color="primary", className="mr-1"
                    )
                ),
            ]
        ),
        dbc.Row([html.Div(id="info"), html.Div(id="info2")]),
        html.Br(),
        dbc.Row(
            html.Div([dash_table.DataTable(id="datatable-interactivity")], id="table"),
            justify="center",
        ),
        dbc.Modal(
            [
                dbc.ModalHeader(id="contract_id"),
                dbc.ModalBody(id="contract_content"),
                dbc.ModalFooter(
                    dbc.Button("Close", id="close", className="ml-auto", n_clicks=0)
                ),
            ],
            id="modal",
            is_open=False,
            size="xl",
        ),
        dbc.Toast(
            "Izberi Id Posla za vec informacij o poslu.",
            header="Nasvet",
            is_open=True,
            dismissable=True,
            icon="info",
            # top: 66 positions the toast below the navbar
            style={"position": "fixed", "top": 66, "left": 10, "width": 200},
        ),
    ],
    className="p-5",
)


@app.callback(
    Output("table", "children"),
    [Input("refresh", "n_clicks")],
    [
        State("tip", "value"),
        State("obcina", "value"),
        State("year", "value"),
    ],
)
def query_database(n, data_type, obcina, year):
    if n is None:
        raise PreventUpdate
    try:
        return datatable(data_type, obcina, year)
    except Exception as e:
        return str(e)


@app.callback(
    [
        Output("modal", "is_open"),
        Output("contract_id", "children"),
        Output("contract_content", "children"),
    ],
    [Input("datatable-interactivity", "active_cell"), Input("close", "n_clicks")],
    [State("year", "value")],
)
def query_database(active_cell, n1, year):
    ctx = dash.callback_context
    if ctx.triggered[0]["prop_id"].split(".")[0] == "close":
        return False, None, None
    elif active_cell:
        if active_cell["column_id"] == "id":
            row_id = active_cell["row_id"]

            title = "Posel {}".format(row_id)
            return True, title, contract_info(row_id, year)

    return False, None, None


if __name__ == "__main__":
    app.run_server(debug=True)

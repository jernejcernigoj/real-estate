# real-estate-dash
dashboard for checking real estate deals in Slovenia accessible at:

https://real-estate-slo.herokuapp.com/



## development
after cloning the repo make sure you have `pre-commit` installed

```pip install pre-commit```

then run `pre-commit install` in the project folder to install the hooks

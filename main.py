# -*- coding: utf-8 -*-
import os

import pandas as pd

MAPPING = {
    "Vrsta kupoprodajnega posla": 24,
    "Vključenost DDV": 64,
    "Stopnja DDV": 29,
    "Posredovanje nepremičninske agencije": 64,
    "Vrsta akta": 83,
    "Tržnost posla": 88,
    "Vrsta zemljišča": 3,
    "Vrsta trajnega nasada": 27,
    "Stopnja DDV parcele": 87,
    "Evidentiranost dela stavbe": 78,
    "Vrsta dela stavbe": 6,
    "Stavba je dokončana": 64,
    "Gradbena faza": 5,
    "Novogradnja": 64,
    "Atrij": 64,
    "Stopnja DDV dela stavbe": 87,
    "Položaj pogodbene stranke": 13,
    "Pravno-organizacijska oblika": 7,
    "Državljanstvo/ Država sedeža": 14,
    "Vrsta najemnega posla": 25,
    "Vključenost obratovalnih stroškov v najemnino": 64,
    "Vrsta oddanih prostorov": 30,
    "Opremljenost oddanih prostorov": 64,
    "Mikrolokacija oddanih prostorov": 42,
    "Izložba": 64,
    "Nakupovalni center": 64,
    "Površina oddanih prostorov enaka delu stavbe": 64,
    "Gostinski vrt": 64,
    "Vključenost gostinskega vrta v najemnino": 64,
}

ZEMLJISCA_MAPPING = {
    "id": "ID Posla",
    "id_ko": "Šifra KO",
    "kataster": "Ime KO",
    "obcina": "Občina",
    "parcela": "Parcelna številka",
    "delez": "Prodani delež parcele",
    "povrsina": "Površina parcele",
    "datum": "Datum uveljavitve",
    "datum_pogodbe": "Datum sklenitve pogodbe",
    "cena": "Pogodbena cena / Odškodnina",
}

STAVBE_MAPPING = {
    "id": "ID Posla",
    "id_ko": "Šifra KO",
    "kataster": "Ime KO",
    "obcina": "Občina",
    "naselje": "Naselje",
    "ulica": "Ulica",
    "stevilka": "Hišna številka",
    "dodatek": "Dodatek HŠ",
    "del stavbe": "Številka stanovanja ali poslovnega prostora",
    "delez": "Prodani delež dela stavbe",
    "povrsina": "Površina dela stavbe",
    "datum": "Datum uveljavitve",
    "datum_pogodbe": "Datum sklenitve pogodbe",
    "cena": "Pogodbena cena / Odškodnina",
}

dirname = os.path.dirname(__file__)


def get_data(_type, year):
    filepath = "data/SLO/ETN_SLO_KUP_{}_20210522/".format(year)
    mypath = os.path.join(dirname, filepath)
    posli = pd.read_csv(
        mypath + "ETN_SLO_KUP_{}_posli_20210522.csv".format(year),
        delimiter=";",
        decimal=",",
    )
    if _type == "zemljisca":
        zemljisca = pd.read_csv(
            mypath + "ETN_SLO_KUP_{}_zemljisca_20210522.csv".format(year),
            delimiter=";",
            decimal=",",
        )
        results = zemljisca.join(posli.set_index("ID Posla"), on="ID Posla")
        results = results[list(ZEMLJISCA_MAPPING.values())]
        results.columns = list(ZEMLJISCA_MAPPING.keys())
        datatable_columns = [
            {"name": i, "id": j}
            for i, j in zip(ZEMLJISCA_MAPPING.values(), ZEMLJISCA_MAPPING.keys())
        ]
    elif _type == "stavbe":
        zemljisca = pd.read_csv(
            mypath + "ETN_SLO_KUP_{}_delistavb_20210522.csv".format(year),
            delimiter=";",
            decimal=",",
        )
        results = zemljisca.join(posli.set_index("ID Posla"), on="ID Posla")
        results = results[list(STAVBE_MAPPING.values())]
        results.columns = list(STAVBE_MAPPING.keys())
        datatable_columns = [
            {"name": i, "id": j}
            for i, j in zip(STAVBE_MAPPING.values(), STAVBE_MAPPING.keys())
        ]
    else:
        raise ValueError
    results["datum"] = pd.to_datetime(results.datum, format="%d.%m.%Y").dt.date
    results["datum_pogodbe"] = pd.to_datetime(
        results.datum_pogodbe, format="%d.%m.%Y"
    ).dt.date
    return results, datatable_columns


def obcine_options():
    mypath = os.path.join(dirname, "data/obcine.csv")
    obcine = list(pd.read_csv(mypath, usecols=[1])["Občina"])
    records = []
    for obcina in obcine:
        records.append({"label": str(obcina).capitalize(), "value": obcina})
    return records


def fiter_by_id(id, year):
    filepath = "data/SLO/ETN_SLO_KUP_{}_20210522/".format(year)
    mypath = os.path.join(dirname, filepath)
    posli = pd.read_csv(
        mypath + "ETN_SLO_KUP_{}_posli_20210522.csv".format(year),
        delimiter=";",
        decimal=",",
    )
    posel = posli.loc[posli["ID Posla"] == id].copy()

    zemljisca = pd.read_csv(
        mypath + "ETN_SLO_KUP_{}_zemljisca_20210522.csv".format(year),
        delimiter=";",
        decimal=",",
    )
    zemljisce = zemljisca.loc[zemljisca["ID Posla"] == id].copy()
    savbe = pd.read_csv(
        mypath + "ETN_SLO_KUP_{}_delistavb_20210522.csv".format(year),
        delimiter=";",
        decimal=",",
    )
    stavba = savbe.loc[savbe["ID Posla"] == id].copy()

    sifrant = pd.read_csv(
        mypath + "etn_sifranti_20210522.csv",
        delimiter=";",
        decimal=",",
    )

    posel = apply_mapping(posel, sifrant).dropna(axis=1, how="all")
    zemljisce = apply_mapping(zemljisce, sifrant).dropna(axis=1, how="all")
    stavba = apply_mapping(stavba, sifrant).dropna(axis=1, how="all")

    return posel, zemljisce, stavba


def apply_mapping(df, sifrant):
    for key, index in MAPPING.items():
        if key in list(df.columns):
            for i, row in df.iterrows():
                value = sifrant[(sifrant.ID == index) & (sifrant["Numerična vrednost"] == row[key])]
                if not value.empty:
                    df.loc[i, key] = value["Opis"].values[0]
    return df


if __name__ == "__main__":
    df, cols = get_data("stavbe", 2019)
    records = obcine_options()

    posel, zemljisce, stavba = fiter_by_id(523633, 2021)
